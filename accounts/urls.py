from django.urls import path
from accounts.views import user_login
from accounts.views import user_logout
from accounts.views import signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("login/", user_login, name="login"),
]
