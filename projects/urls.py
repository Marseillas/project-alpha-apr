from django.urls import path
from projects.views import list_projects
from projects.views import My_list_projects
from projects.views import show_project


urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("mine/", My_list_projects, name="My_list_projects"),
    path("projects/", list_projects, name="list_projects"),
]
